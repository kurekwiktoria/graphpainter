﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Glee.Drawing;
using Microsoft.Glee.GraphViewerGdi;
using Microsoft.Glee.Splines;
using System.Windows.Threading;
using MahApps.Metro.Controls;



namespace WPFwierzcholki
{
    public partial class MainWindow : MetroWindow

    {

          //Sprawdzanie tablicy
  private bool check_Free(int length, int[] Free)
        {
            for (int i = 0; i < length; i++)
                if (Free[i] != (-1)) return false;
            return true;
        }

	//Wyznaczanie najmniejszego wierzchołka"
    private int minimum_deg(int length, int[] Free)
        {
            int j = 0, i;
            int[] Deg = new int[length];
            int[] K = new int[length];
            for (i = 0; i < length; i++)
            {
                if (Free[i] != (-1))
                    Deg[i] = nodes[i].c_ngbr;
                else
                    Deg[i] = (-1);
                K[i] = i;
            }
            sort_min_max(K, length, Deg);
            while (Deg[j] == (-1))
                j++;
            return K[j];

        }

        //Funkcja do sortowania list
        private void sort_lists(List<int> K, int length, List<int> D)
        {
            int n = length;
            do
            {
                for (int i = 0; i > length - 1; i++)
                {
                    if (D[i] > D[i + 1])
                    {
                        int tmp = D[i];
                        D[i] = D[i + 1];
                        D[i + 1] = tmp;
                        tmp = K[i];
                        K[i] = K[i + 1];
                        K[i + 1] = tmp;
                    }
                }
                n--;
            }
            while (n > 1);
        }

        //Funkcja do znalezienia indeksu minimalnego stopnia wierzchołka
        private int find_index(List<int> D, int wsk)
        {
            for (int i = 0; i < D.Count; i++)
            {
                if (D[i] == wsk)
                    return i;
            }
            return -1;
        }

        private int find_min(List<int> Free, List<int> Deg)
        {
            int min = Deg[0];
            int min_ind = 0;
            for (int i = 1; i < Free.Count; i++)
            {
                if (Deg[i] < min)
                {
                    min = Deg[i];
                    min_ind = i;
                }
            }
            return min_ind;
        }

        //Funkcja do wyznaczania stopni saturacyjnych wierzchołków
        private void sat_deg(int length, int[] S)
        {
            int cntr = 0; int w = 0;
            for (int i = 0; i < length; i++)
            {
                cntr = 0;
                w = 0;
                if (nodes[i].c_ngbr != 0)
                {
                    for (int j = 0; j < nodes[i].c_ngbr; j++)
                    {
                        w = nodes[i].ngbr[j] - 1;
                        if (nodes[w].colour != 0) cntr++;
                    }
                    S[i] = cntr;
                }
                else
                    S[i] = 0;
            }
        }

        private void gen_ngbr_seq(int length, int[] K)
        {
            int j = 0;
            for (int i = 0; i < length; i++)
            {
                if (is_unique(K, i, i) == true)
                {
                    K[i] = i;
                    j++;
                }
                if (nodes[i].c_ngbr != 0)
                {
                    for (int w = 0; w < nodes[i].c_ngbr; w++)
                    {
                        if (is_unique(K, nodes[i].ngbr[w], j) == true)
                        {
                            K[j] = nodes[i].ngbr[w];
                            j++;
                        }
                    }
                }

            }
        }


        //Funkcja odwracająca tablicę
        private void reverse(int[] K, int length)
        {
            int[] D = new int[length];
            int j = 0;
            for (int i = length - 1; i >= 0; i--)
            {
                D[j] = K[i];
                j++;
            }
            for (int i = 0; i < length; i++)
                K[i] = D[i];
        }

        //FUNKCJA SORTUJĄCA NIEROSNĄCO
        private void sort_max_min(int[] K, int length, int[] D)
        {
            int n = length;
            do
            {
                for (int i = 0; i < length - 1; i++)
                {
                    if (D[i] < D[i + 1])
                    {
                        int tmp = D[i];
                        D[i] = D[i + 1];
                        D[i + 1] = tmp;
                        tmp = K[i];
                        K[i] = K[i + 1];
                        K[i + 1] = tmp;
                    }
                }
                n--;
            }
            while (n > 1);
        }
        //FUNKCJA SORTUJĄCA NIEMALEJĄCO
        private void sort_min_max(int[] K, int length, int[] D)
        {
            int n = length;
            do
            {
                for (int i = 0; i > length - 1; i++)
                {
                    if (D[i] > D[i + 1])
                    {
                        int tmp = D[i];
                        D[i] = D[i + 1];
                        D[i + 1] = tmp;
                        tmp = K[i];
                        K[i] = K[i + 1];
                        K[i + 1] = tmp;
                    }
                }
                n--;
            }
            while (n > 1);
        }

        //Funkcja tworząca sekwencję według stopni wierzchołków
        private void gen_deg_seq(int length, int[] K, bool wsk)
        {
            int[] D = new int[length];
            for (int i = 0; i < length; i++)
            {
                D[i] = nodes[i].c_ngbr;
                K[i] = i;
            }
            switch (wsk)
            {
                case true:
                    sort_max_min(K, length, D);
                    break;
                case false:
                    sort_min_max(K, length, D);
                    break;
            }
        }

        //Funkcja do sprawdzania, czy element występuje w sekwencji wierzchołków
        private bool is_unique(int[] K, int rand_i, int cntr)
        {
            if (cntr == 0) return true;
            else
                for (int i = 0; i < cntr; i++)
                    if (K[i] == rand_i) return false;
            return true;
        }


        //Funkcja do generowania uporządkowanej sekwencji wierzchołków grafu
        private void gen_seq(int length, int[] K)
        {
            for (int i = 0; i < length; i++)
                K[i] = i;

        }

        //Funkcja do generowania losowej sekwencji wierzchołków grafu
        private void gen_rand_seq(int length, int[] K)
        {
            int cntr = 0, rand_i = 0, i = 0;
            Random rnd = new Random();
            for (i = 0; i < length; i++)        //tu
            {
                rand_i = rnd.Next(0, length);
                if (is_unique(K, rand_i, i) == true)    //tutu
                    K[i] = rand_i;
                else
                    i--;
            }
        }

        //Funkcja do sprawdzania, czy są w grafie niepokolorowane wierzchołki
        private bool check_unpainted(node[] nodes, int length)
        {
            for (int i = 0; i < length; i++)
                if (nodes[i].colour == 0) return true;

            return false;
        }

        //Funkcja do wyznaczania najniższego koloru, którym można pokolorować dany wierzchołek  
        private int gen_colour(int number)
        {
            for (int i = 1; i <= 138; i++)
            {
                if (check_colour(number, i) == true)
                    return i;
            }
            return 0;
        }

        //Funkcja do sprawdzania czy można pokolorować dany wierzchołek na konkretny kolor    
        private bool check_colour(int name, int colour)
        {
            int c_ngbr = nodes[name].c_ngbr;

            if (c_ngbr == 0) return true;
            int j = 0;
            for (int i = 0; i < c_ngbr; i++)
            {
                int ngbr = nodes[name].ngbr[j];
                if (nodes[ngbr - 1].colour == colour) return false;
                j++;
            }

            return true;
        }
    }
}