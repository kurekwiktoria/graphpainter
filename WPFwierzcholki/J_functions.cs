using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Glee.Drawing;
using Microsoft.Glee.GraphViewerGdi;
using Microsoft.Glee.Splines;
using System.Windows.Threading;
using MahApps.Metro.Controls;



namespace WPFwierzcholki
{
    public partial class MainWindow : MetroWindow

    {
        private void gen_struct(ref string filename)
        {
            //odczytywanie wszystkich lini z pliku
            string[] lines = System.IO.File.ReadAllLines(@filename);
            len = lines.Length;
            K = new int[len];
            int z = 0;
            int[,] represent = new int[len, len];

            //przyporządkowywanie elementów do tablicy int
            for (int w = 0; w < len; w++)
            {
                z = 0;
                for (int c = 0; c < lines[w].Length; c++)
                {
                    if (lines[w][c] == '1' || lines[w][c] == '0')
                    {
                        represent[w, z] = (int)(lines[w][c] - '0');
                        z++;
                    }
                }
            }

            //tworzenie wierzcholkow
            nodes = new node[len];

            for (int c = 0; c < len; c++)
            {
                //nadanie nazwy
                nodes[c].name = (c + 1).ToString();

                //utworzenie listy
                nodes[c].ngbr = new List<int>();

                for (int w = 0; w < len; w++)
                {
                    if (represent[w, c] == 1)
                    {
                        nodes[c].ngbr.Add(w + 1);
                        nodes[c].c_ngbr++;
                    }
                }
            }
            gen_graph(nodes, len, represent);
        }

        // private void color_graph( )

        private void gen_graph(node[] nodes, int c_nodes, int[,] represent)
        {
            //Graph graph = new Graph("graph");

            //tworzenie wierzcholkow
            for (int i = 0; i < c_nodes; i++)
            {
                graph.AddNode(nodes[i].name);
            }

            //tworzenie krawędzi
            for (int c = 0; c < c_nodes; c++)
                for (int w = 0; w < c_nodes; w++)
                {
                    if (represent[w, c] == 1)
                    {
                        graph.AddEdge((c + 1).ToString(), (w + 1).ToString()).Attr.ArrowHeadAtTarget = ArrowStyle.None;
                        represent[w, c] = 0;
                        represent[c, w] = 0;
                    }
                }
            this.gViewer.Graph = graph;
        }
    }
}