﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Glee.Drawing;
using Microsoft.Glee.GraphViewerGdi;
using Microsoft.Glee.Splines;
using System.Windows.Threading;
using MahApps.Metro.Controls;



namespace WPFwierzcholki
{
    public partial class MainWindow : MetroWindow
    {

        //Kolorowanie zachłanne:
        private void KolorujZachlannie(int length, int[] K)
        {
            int k = 0;
            for (int i = 0; i < length; i++)
            {
                k = K[i];
                nodes[k].colour = gen_colour(k);
            }
        }

        //Kolorowanie RS:
        private void KolorujRS(int length, int[] K)
        {
            gen_rand_seq(len, K);
            KolorujZachlannie(len, K);
        }

        //Kolorowanie z wymianą
        private void KolorujZWymiana(int length, int[] K)
        {
            KolorujZachlannie(length, K);
        }

        //Kolorowanie SLI
        private void KolorujSLI(int length, int[] K)
        {
            gen_deg_seq(length, K, false);
            reverse(K, length);
            KolorujZWymiana(length, K);
        }
        //Kolorowanie LFI
        private void KolorujLFI(int length, int[] K)
        {
            gen_deg_seq(length, K, true);
            KolorujZWymiana(length, K);
        }
        //Kolorowanie RSI
        private void KolorujRSI(int length, int[] K)
        {
            gen_rand_seq(length, K);
            KolorujZWymiana(length, K);
        }

        //Kolorowanie SL
        private void KolorujSL(int length, int[] K)
        {
            gen_deg_seq(length, K, false);
            reverse(K, length);
            KolorujZachlannie(length, K);
        }


        //Kolorowanie LF 
        private void KolorujLF(int length, int[] K)
        {
            gen_deg_seq(length, K, true);
            KolorujZachlannie(length, K);
        }

        //Kolorowanie CS
        private void KolorujCS(int length, int[] K)
        {
            gen_ngbr_seq(length, K);
            KolorujZachlannie(length, K);
        }

        //Kolorowanie SLF
        private void KolorujSLF(int length, int[] K)
        {
            int[] S = new int[length];
            int[] D = new int[length];
            while (check_unpainted(nodes, length) == true)
            {
                sat_deg(length, S);
                gen_seq(length, D);
                sort_max_min(D, length, S);
                int wsk = 0;
                int j = 0;
                while (wsk == 0)
                {
                    if (nodes[D[j]].colour == 0)
                    {
                        nodes[D[j]].colour = gen_colour(D[j]);
                        wsk = 1;
                    }
                    else
                        j++;
                }
            }

        }

        //Kolorowanie GIS
        private void KolorujGIS(int length)
        {
            List<int> Free = new List<int>();
            List<int> Deg = new List<int>();
            int i;
            while (check_unpainted(nodes, length) == true)
            {
                for (i = 0; i < length; i++)
                    if (nodes[i].colour == 0)
                    {
                        Free.Add(i);
                        Deg.Add(nodes[i].c_ngbr);
                    }
                i = 0;
                while (Free.Count != 0)
                {                                                                               //tuu
                    sort_lists(Free, Free.Count, Deg);
                    nodes[Free[i]].colour = gen_colour(Free[i]);
                    for (int j = 0; j < nodes[Free[i]].c_ngbr; j++)
                    {
                        int k = find_index(Free, nodes[Free[i]].ngbr[j]);
                        if (k != (-1))
                        {
                            Free.RemoveAt(k);
                            Deg.RemoveAt(k);
                        }
                        i++;                                                                        //koniec tuu
                    }
                    Free.RemoveAt(0);
                    Deg.RemoveAt(0);
                }
            }
        }



          //Kolorowanie GIS v2"
  private void KolorujGISv2(int length)
        {

            while (check_unpainted(nodes, length) == true)
            {
                int v = 0;
                int[] Free = new int[length];
                for (int i = 0; i < length; i++)
                {
                    if (nodes[i].colour != 0)
                        Free[i] = i;
                    else
                        Free[i] = (-1);
                }
                while (check_Free(length, Free) == false)
                {
                    v = minimum_deg(length, Free);
                    nodes[v].colour = gen_colour(v);
                    Free[v] = (-1);
                    //for (int j = 0; j < nodes[v].c_ngbr; j++)
                      //  Free[j - 1] = (-1);

                }

            }

        }

        //Funkcja do zwracania kolejnego możliwego koloru do pokolorowania
        private Microsoft.Glee.Drawing.Color ret_colour(int clr)
            {
                switch (clr)
                {
                    case 1:
                        return Microsoft.Glee.Drawing.Color.Aqua;
                        break;
                    case 2:
                        return Microsoft.Glee.Drawing.Color.Chocolate;
                        break;
                    case 3:
                        return Microsoft.Glee.Drawing.Color.Chartreuse;
                        break;
                    case 4:
                        return Microsoft.Glee.Drawing.Color.BlueViolet;
                        break;
                    case 5:
                        return Microsoft.Glee.Drawing.Color.Fuchsia;
                        break;
                    case 6:
                        return Microsoft.Glee.Drawing.Color.DeepSkyBlue;
                        break;
                    case 7:
                        return Microsoft.Glee.Drawing.Color.Crimson;
                        break;
                    case 8:
                        return Microsoft.Glee.Drawing.Color.Olive;
                        break;
                    case 9:
                        return Microsoft.Glee.Drawing.Color.Blue;
                        break;
                    case 10:
                        return Microsoft.Glee.Drawing.Color.Gold;
                        break;
                    case 11:
                        return Microsoft.Glee.Drawing.Color.DarkSalmon;
                        break;
                    case 12:
                        return Microsoft.Glee.Drawing.Color.LawnGreen;
                        break;
                    case 13:
                        return Microsoft.Glee.Drawing.Color.Navy;
                        break;
                    case 14:
                        return Microsoft.Glee.Drawing.Color.MistyRose;
                        break;
                    case 15:
                        return Microsoft.Glee.Drawing.Color.Brown;
                        break;
                    case 16:
                        return Microsoft.Glee.Drawing.Color.AliceBlue;
                        break;
                    case 17:
                        return Microsoft.Glee.Drawing.Color.DarkViolet;
                        break;
                    case 18:
                        return Microsoft.Glee.Drawing.Color.DeepPink;
                        break;
                    case 19:
                        return Microsoft.Glee.Drawing.Color.Coral;
                        break;
                    case 20:
                        return Microsoft.Glee.Drawing.Color.CornflowerBlue;
                        break;
                    case 21:
                        return Microsoft.Glee.Drawing.Color.Cyan;
                        break;
                    case 22:
                        return Microsoft.Glee.Drawing.Color.OliveDrab;
                        break;
                    case 23:
                        return Microsoft.Glee.Drawing.Color.Orange;
                        break;
                    case 24:
                        return Microsoft.Glee.Drawing.Color.Cornsilk;
                        break;
                    case 25:
                        return Microsoft.Glee.Drawing.Color.Magenta;
                        break;
                    case 26:
                        return Microsoft.Glee.Drawing.Color.Maroon;
                        break;
                    case 27:
                        return Microsoft.Glee.Drawing.Color.MediumAquamarine;
                        break;
                    case 28:
                        return Microsoft.Glee.Drawing.Color.MediumBlue;
                        break;
                    case 29:
                        return Microsoft.Glee.Drawing.Color.LightSteelBlue;
                        break;
                    case 30:
                        return Microsoft.Glee.Drawing.Color.LightYellow;
                        break;
                    case 31:
                        return Microsoft.Glee.Drawing.Color.Lime;
                        break;
                    case 32:
                        return Microsoft.Glee.Drawing.Color.LightSalmon;
                        break;
                    case 33:
                        return Microsoft.Glee.Drawing.Color.LightSeaGreen;
                        break;
                    case 34:
                        return Microsoft.Glee.Drawing.Color.DarkOrchid;
                        break;
                    case 35:
                        return Microsoft.Glee.Drawing.Color.DarkOliveGreen;
                        break;
                    case 36:
                        return Microsoft.Glee.Drawing.Color.DarkOrange;
                        break;
                    case 37:
                        return Microsoft.Glee.Drawing.Color.DimGray;
                        break;
                    case 38:
                        return Microsoft.Glee.Drawing.Color.DodgerBlue;
                        break;
                    case 39:
                        return Microsoft.Glee.Drawing.Color.Firebrick;
                        break;
                    case 40:
                        return Microsoft.Glee.Drawing.Color.Bisque;
                        break;
                    case 41:
                        return Microsoft.Glee.Drawing.Color.GreenYellow;
                        break;
                    case 42:
                        return Microsoft.Glee.Drawing.Color.Indigo;
                        break;
                    case 43:
                        return Microsoft.Glee.Drawing.Color.Green;
                        break;
                    case 44:
                        return Microsoft.Glee.Drawing.Color.LightCoral;
                        break;
                    case 45:
                        return Microsoft.Glee.Drawing.Color.LightCyan;
                        break;
                    case 46:
                        return Microsoft.Glee.Drawing.Color.HotPink;
                        break;
                    case 47:
                        return Microsoft.Glee.Drawing.Color.LightSkyBlue;
                        break;
                    case 48:
                        return Microsoft.Glee.Drawing.Color.Goldenrod;
                        break;
                    case 49:
                        return Microsoft.Glee.Drawing.Color.Gray;
                        break;
                    case 50:
                        return Microsoft.Glee.Drawing.Color.MediumVioletRed;
                        break;
                    case 51:
                        return Microsoft.Glee.Drawing.Color.MidnightBlue;
                        break;
                    case 52:
                        return Microsoft.Glee.Drawing.Color.LimeGreen;
                        break;
                    case 53:
                        return Microsoft.Glee.Drawing.Color.Linen;
                        break;
                    case 54:
                        return Microsoft.Glee.Drawing.Color.Khaki;
                        break;
                    case 55:
                        return Microsoft.Glee.Drawing.Color.Aquamarine;
                        break;
                    case 56:
                        return Microsoft.Glee.Drawing.Color.IndianRed;
                        break;
                    case 57:
                        return Microsoft.Glee.Drawing.Color.OrangeRed;
                        break;
                    case 58:
                        return Microsoft.Glee.Drawing.Color.Orchid;
                        break;
                    case 59:
                        return Microsoft.Glee.Drawing.Color.PaleGoldenrod;
                        break;
                    case 60:
                        return Microsoft.Glee.Drawing.Color.PaleGreen;
                        break;
                    case 61:
                        return Microsoft.Glee.Drawing.Color.PaleTurquoise;
                        break;
                    case 62:
                        return Microsoft.Glee.Drawing.Color.LightSlateGray;
                        break;
                    case 63:
                        return Microsoft.Glee.Drawing.Color.Lavender;
                        break;
                    case 64:
                        return Microsoft.Glee.Drawing.Color.Peru;
                        break;
                    case 65:
                        return Microsoft.Glee.Drawing.Color.LightPink;
                        break;
                    case 66:
                        return Microsoft.Glee.Drawing.Color.LavenderBlush;
                        break;
                    case 67:
                        return Microsoft.Glee.Drawing.Color.DarkSeaGreen;
                        break;
                    case 68:
                        return Microsoft.Glee.Drawing.Color.DarkSlateBlue;
                        break;
                    case 69:
                        return Microsoft.Glee.Drawing.Color.DarkSlateGray;
                        break;
                    case 70:
                        return Microsoft.Glee.Drawing.Color.FloralWhite;
                        break;
                    case 71:
                        return Microsoft.Glee.Drawing.Color.ForestGreen;
                        break;
                    case 72:
                        return Microsoft.Glee.Drawing.Color.LightBlue;
                        break;
                    case 73:
                        return Microsoft.Glee.Drawing.Color.Purple;
                        break;
                    case 74:
                        return Microsoft.Glee.Drawing.Color.Red;
                        break;
                    case 75:
                        return Microsoft.Glee.Drawing.Color.MintCream;
                        break;
                    case 76:
                        return Microsoft.Glee.Drawing.Color.Yellow;
                        break;
                    case 77:
                        return Microsoft.Glee.Drawing.Color.YellowGreen;
                        break;
                    case 78:
                        return Microsoft.Glee.Drawing.Color.PowderBlue;
                        break;
                    case 79:
                        return Microsoft.Glee.Drawing.Color.MediumPurple;
                        break;
                    case 80:
                        return Microsoft.Glee.Drawing.Color.MediumSeaGreen;
                        break;
                    case 81:
                        return Microsoft.Glee.Drawing.Color.RoyalBlue;
                        break;
                    case 82:
                        return Microsoft.Glee.Drawing.Color.SaddleBrown;
                        break;
                    case 83:
                        return Microsoft.Glee.Drawing.Color.OldLace;
                        break;
                    case 84:
                        return Microsoft.Glee.Drawing.Color.Salmon;
                        break;
                    case 85:
                        return Microsoft.Glee.Drawing.Color.BurlyWood;
                        break;
                    case 86:
                        return Microsoft.Glee.Drawing.Color.CadetBlue;
                        break;
                    case 87:
                        return Microsoft.Glee.Drawing.Color.Beige;
                        break;
                    case 88:
                        return Microsoft.Glee.Drawing.Color.MediumOrchid;
                        break;
                    case 89:
                        return Microsoft.Glee.Drawing.Color.PaleVioletRed;
                        break;
                    case 90:
                        return Microsoft.Glee.Drawing.Color.PapayaWhip;
                        break;
                    case 91:
                        return Microsoft.Glee.Drawing.Color.PeachPuff;
                        break;
                    case 92:
                        return Microsoft.Glee.Drawing.Color.MediumSlateBlue;
                        break;
                    case 93:
                        return Microsoft.Glee.Drawing.Color.MediumSpringGreen;
                        break;
                    case 94:
                        return Microsoft.Glee.Drawing.Color.Tomato;
                        break;
                    case 95:
                        return Microsoft.Glee.Drawing.Color.DarkGoldenrod;
                        break;
                    case 96:
                        return Microsoft.Glee.Drawing.Color.DarkGray;
                        break;
                    case 97:
                        return Microsoft.Glee.Drawing.Color.WhiteSmoke;
                        break;
                    case 98:
                        return Microsoft.Glee.Drawing.Color.DarkGreen;
                        break;
                    case 99:
                        return Microsoft.Glee.Drawing.Color.DarkKhaki;
                        break;
                    case 100:
                        return Microsoft.Glee.Drawing.Color.Moccasin;
                        break;
                    case 101:
                        return Microsoft.Glee.Drawing.Color.MediumTurquoise;
                        break;
                    case 102:
                        return Microsoft.Glee.Drawing.Color.Plum;
                        break;
                    case 103:
                        return Microsoft.Glee.Drawing.Color.Pink;
                        break;
                    case 104:
                        return Microsoft.Glee.Drawing.Color.SandyBrown;
                        break;
                    case 105:
                        return Microsoft.Glee.Drawing.Color.SeaGreen;
                        break;
                    case 106:
                        return Microsoft.Glee.Drawing.Color.RosyBrown;
                        break;
                    case 107:
                        return Microsoft.Glee.Drawing.Color.Sienna;
                        break;
                    case 108:
                        return Microsoft.Glee.Drawing.Color.Turquoise;
                        break;
                    case 109:
                        return Microsoft.Glee.Drawing.Color.Violet;
                        break;
                    case 110:
                        return Microsoft.Glee.Drawing.Color.SlateBlue;
                        break;
                    case 111:
                        return Microsoft.Glee.Drawing.Color.SeaShell;
                        break;
                    case 112:
                        return Microsoft.Glee.Drawing.Color.SpringGreen;
                        break;
                    case 113:
                        return Microsoft.Glee.Drawing.Color.SkyBlue;
                        break;
                    case 114:
                        return Microsoft.Glee.Drawing.Color.Silver;
                        break;
                    case 115:
                        return Microsoft.Glee.Drawing.Color.NavajoWhite;
                        break;
                    case 116:
                        return Microsoft.Glee.Drawing.Color.LightGoldenrodYellow;
                        break;
                    case 117:
                        return Microsoft.Glee.Drawing.Color.LightGreen;
                        break;
                    case 118:
                        return Microsoft.Glee.Drawing.Color.SteelBlue;
                        break;
                    case 119:
                        return Microsoft.Glee.Drawing.Color.Tan;
                        break;
                    case 120:
                        return Microsoft.Glee.Drawing.Color.Teal;
                        break;
                    case 121:
                        return Microsoft.Glee.Drawing.Color.Thistle;
                        break;
                    case 122:
                        return Microsoft.Glee.Drawing.Color.Wheat;
                        break;
                    case 123:
                        return Microsoft.Glee.Drawing.Color.SlateGray;
                        break;
                    case 124:
                        return Microsoft.Glee.Drawing.Color.Snow;
                        break;
                    case 125:
                        return Microsoft.Glee.Drawing.Color.Ivory;
                        break;
                    case 126:
                        return Microsoft.Glee.Drawing.Color.Gainsboro;
                        break;
                    case 127:
                        return Microsoft.Glee.Drawing.Color.LemonChiffon;
                        break;
                    case 128:
                        return Microsoft.Glee.Drawing.Color.DarkCyan;
                        break;
                    case 129:
                        return Microsoft.Glee.Drawing.Color.AntiqueWhite;
                        break;
                    case 130:
                        return Microsoft.Glee.Drawing.Color.DarkBlue;
                        break;
                    case 131:
                        return Microsoft.Glee.Drawing.Color.DarkMagenta;
                        break;
                    case 132:
                        return Microsoft.Glee.Drawing.Color.Azure;
                        break;
                    case 133:
                        return Microsoft.Glee.Drawing.Color.DarkRed;
                        break;
                    case 134:
                        return Microsoft.Glee.Drawing.Color.DarkTurquoise;
                        break;
                    case 135:
                        return Microsoft.Glee.Drawing.Color.BlanchedAlmond;
                        break;
                    case 136:
                        return Microsoft.Glee.Drawing.Color.LightGray;
                        break;
                    case 137:
                        return Microsoft.Glee.Drawing.Color.GhostWhite;
                        break;
                    case 138:
                        return Microsoft.Glee.Drawing.Color.Honeydew;
                        break;
                }
                return Microsoft.Glee.Drawing.Color.White;
            }
        }
    }