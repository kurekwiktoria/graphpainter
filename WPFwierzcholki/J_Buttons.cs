﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Glee.Drawing;
using Microsoft.Glee.GraphViewerGdi;
using Microsoft.Glee.Splines;
using System.Windows.Threading;
using MahApps.Metro.Controls;



namespace WPFwierzcholki
{
    public partial class MainWindow : MetroWindow
    {
        //Plik -----> OPEN -----> CLICK
        private void Open_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();

            // Set filter for file extension and default file extension
            dlg.DefaultExt = ".txt";
            dlg.Filter = "Text documents (.txt)|*.txt";

            // Display OpenFileDialog by calling ShowDialog method
            Nullable<bool> result = dlg.ShowDialog();

            // Get the selected file name and display in a TextBox
            if (result == true)
            {
                // Open document
                string filename = dlg.FileName;
                gen_struct(ref filename);
            }
        }

        //ZASTOSUJ
        private void Button_Apply(object sender, RoutedEventArgs e)
        {

            //wyczyszczenie kolorów
            for (int i = 0; i < len; i++)
            {
                graph.FindNode((i + 1).ToString()).Attr.Fillcolor = Microsoft.Glee.Drawing.Color.White;
                nodes[i].colour = 0;
            }

            //wywołanie odpowiednich funkcji
            switch (Methods_List.Text)
            {
                case "Koloruj zachłannie":
                    gen_seq(len, K);
                    KolorujZachlannie(len, K);
                    break;
                case "Koloruj z wymianą":
                    gen_rand_seq(len, K);
                    KolorujZWymiana(len, K);
                    break;
                case "Koloruj RS":
                    KolorujRS(len, K);
                    break;
                case "Koloruj LF":
                    KolorujLF(len, K);
                    break;
                case "Koloruj SL":
                    KolorujSL(len, K);
                    break;
                case "Koloruj RSI":
                    KolorujRSI(len, K);
                    break;
                case "Koloruj LFI":
                    KolorujLFI(len, K);
                    break;
                case "Koloruj SLI":
                    KolorujSLI(len, K);
                    break;
                case "Koloruj CS":
                    gen_rand_seq(len, K);
                    KolorujZWymiana(len, K);
                    break;
                case "Koloruj SLF":
                    KolorujSLF(len, K);
                    break;
                case "Koloruj GIS":
                    gen_rand_seq(len, K);
                    KolorujZWymiana(len, K);
                    break;
                default:
                    return;
            }

            //USTAWIENIE KOLORU
            for (int i = 0; i < len; i++)
            {
                try
                {
                    graph.FindNode((i + 1).ToString()).Attr.Fillcolor = ret_colour(nodes[i].colour);
                }
                catch (System.NullReferenceException)
                {
                    System.Windows.Forms.MessageBox.Show("Brak grafu!", "Błąd", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                    return;
                }
            }



            this.gViewer.Graph = graph;

        }

        //Wyczyść kolory
        private void Button_Clear_Col(object sender, RoutedEventArgs e)
        {
            try
            {
                graph.FindNode("1").Attr.Fillcolor = Microsoft.Glee.Drawing.Color.White;
            }
            catch (System.NullReferenceException)
            {
                return;
            }


            for (int i = 0; i < len; i++)
            {
                graph.FindNode((i + 1).ToString()).Attr.Fillcolor = Microsoft.Glee.Drawing.Color.White;
                nodes[i].colour = 0;
            }
            this.gViewer.Graph = graph;
        }

        //WYCZYSC GRAF
        private void Button_Clear(object sender, RoutedEventArgs e)
        {

            graph = new Graph("graph");
            this.gViewer.Graph = graph;

            /* USTAWIENIA GRAFU
                g.GraphAttr.LayerDirection = LayerDirection.LR;
                g.GraphAttr.Orientation = Microsoft.Glee.Drawing.Orientation.Portrait;
                g.GraphAttr.Border = 2;

                g.GraphAttr.AspectRatio = 1;

                g.GraphAttr.NodeAttr.FontName = "Tahoma";
                g.GraphAttr.NodeAttr.Fontsize = 5;
                g.GraphAttr.NodeAttr.Shape = Microsoft.Glee.Drawing.Shape.Box;

                g.GraphAttr.EdgeAttr.FontName = "Tahoma";
                g.GraphAttr.EdgeAttr.Fontsize = 5;
                g.GraphAttr.EdgeAttr.Separation = 1000;
                g.GraphAttr.EdgeAttr.ArrowHeadAtTarget = ArrowStyle.Normal;
                */
        }

        //Pomoc -----> Obsługa
        private void Service_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.MessageBox.Show(" 1. Program wczytuje graf o postaci macierzowej, aby to zrobić należy wybrać: \n " +
                "Plik ---> Otwórz, a następnie wybierz plik txt. Graf zostanie od razu wyświetlony.\n" +
                "2. Za pomocą listy z metodami kolorowania grafu, wybierz jedną i kliknij zastosuj.\n" +
                "3. Przycisk wyczyść kolory usuwa wszystkie kolory grafu, aby móc pokolorować je ponownie\n" +
                "4. Przycisk wyczyść graf całkowicie usuwa graf, aby później można było wczytać nowy graf z pliku.\n ", "Obsługa", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information);
        }

        //Pomoc -----> O autorach
        private void Author_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.MessageBox.Show(" Wykonali: Wiktoria Kurek, Anita Kowalczyk, Jerzy Gębala\n\n" +
                " Program został wykonany na potrzeby przedmiotu\n Metody Programowania.\n\n" +
                " Informatyka 16/17 PK WIEiK ", "O Autorach", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information);
        }


    }
}