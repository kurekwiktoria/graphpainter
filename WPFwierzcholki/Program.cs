﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPFwierzcholki
{

    class Eloelo
    {
        static int queens = 0;
        static int[] row;
        static System.IO.StreamWriter file = new System.IO.StreamWriter(@"G:\Download\WriteText.txt");
        static int N;

        public static void ELO()
        {
            Console.WriteLine("Witaj w programie do rozwiazywania problemu N-hetmanow.");
            Console.WriteLine("Wprowadz liczbę N:");
            N = Int32.Parse(Console.ReadLine());
            N++;
            row = new int[N];
            new_queen(1);
            Console.WriteLine($"Wypisałem {queens} kombinacji.");
            file.WriteLine($"Wypisałem {queens} kombinacji.");
        }

        static void write(int[] row)
        {
            for (int w = 1; w < N; w++)
            {
                for (int k = 1; k < N; k++)
                {
                    if (w == row[k])
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.Write("H ");
                        file.Write("H ");
                        Console.ForegroundColor = ConsoleColor.Gray;
                    }
                    else
                    {
                        Console.Write("0 ");
                        file.Write("0 ");
                    }


                }
                Console.Write("\n");
                file.Write("\n");
            }
            Console.WriteLine();
            file.Write("\n");
        }

        static void new_queen(int line)
        {
            if ( line > N-1 )
            {
                queens++;
                write(row);

            }
            else
            {
                for (int k = 1; k < N; k++)
                    if (row[k] == 0)
                    {
                        bool condition = true;
                        for (int j = 1; j < N; j++)
                        {
                            if (row[j] != 0 && Math.Abs(row[j] - line) == Math.Abs(j - k))
                            {
                                condition = false;
                                break;
                            }
                        }

                        if (condition)
                        {
                            row[k] = line;
                            new_queen(line + 1);
                            row[k] = 0;
                        }
                    }
            }
        }

        static void write_it(int[,] chess)
        {
            Console.ForegroundColor = ConsoleColor.White;
            for (int w = 0; w < 8; w++)
            {
                for (int i = 0; i < 8; i++)
                {
                    if (chess[w, i] == 1)
                    {
                        Console.ForegroundColor = ConsoleColor.Yellow;
                        Console.Write($"{Convert.ToInt32(chess[w, i])} ");
                        Console.ForegroundColor = ConsoleColor.White;

                    }
                    else
                    {
                        if (chess[w, i] == 5)
                        {
                            Console.ForegroundColor = ConsoleColor.Green;
                            Console.Write($"{Convert.ToInt32(chess[w, i])} ");
                            Console.ForegroundColor = ConsoleColor.White;
                        }
                        else
                            Console.Write($"{Convert.ToInt32(chess[w, i])} ");
                    }
                }
                Console.Write("\n");
            }
        }

    }
}

